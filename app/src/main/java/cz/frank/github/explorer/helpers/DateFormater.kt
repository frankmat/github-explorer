package cz.frank.github.explorer.helpers

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun transformDate(inputFormat: String, outputFormat: String, input: String): String {
    val dateFormat = SimpleDateFormat(inputFormat, Locale.getDefault())
    var date = input
    try {
        val newDate = dateFormat.parse(input)
        date = SimpleDateFormat(outputFormat, Locale.getDefault()).format(
            newDate?.time ?: ""
        )
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return date
}
