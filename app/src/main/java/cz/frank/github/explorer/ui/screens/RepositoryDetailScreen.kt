package cz.frank.github.explorer.ui.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.frank.github.explorer.R
import cz.frank.github.explorer.helpers.DimensionsUntil
import cz.frank.github.explorer.helpers.Resource
import cz.frank.github.explorer.ui.Tabs
import cz.frank.github.explorer.ui.model.Branch
import cz.frank.github.explorer.ui.model.Commit
import cz.frank.github.explorer.ui.theme.AppTheme
import cz.frank.github.explorer.viewmodels.RepositoryViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel

@Destination
@Composable
fun RepositoryDetailScreenHolder(
    navigator: DestinationsNavigator,
    user: String,
    repository: String,
    viewModel: RepositoryViewModel = getViewModel()
) {
    RepositoryDetailScreen(user, repository, viewModel.model) { navigator.popBackStack() }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalPagerApi::class)
@Composable
fun RepositoryDetailScreen(
    user: String,
    repo: String,
    model: RepositoryViewModel.RepositoryScreenModel,
    onBackClick: () -> Unit
) {
    with(model) {

        BackHandler(true) {
            popBackStack()
            onBackClick()
        }
        LaunchedEffect(Unit) {
            initSearch(repo, user)
        }
        BoxWithConstraints {
            val isLandScape by remember {
                derivedStateOf { maxWidth > DimensionsUntil.Portrait.width }
            }
            val tabPages = listOf<@androidx.compose.runtime.Composable () -> Unit>(
                {
                    CommitsSection(commits = commits, showHeadline = isLandScape) {
                        onRefreshCommitsButtonClick(
                            repo,
                            user
                        )
                    }
                },
                {

                    BranchesSection(branches = branches, showHeadline = isLandScape) {
                        onRefreshBranchesButtonClick(
                            repo,
                            user
                        )

                    }
                }
            )

            val pages = listOf(Tabs.Commits, Tabs.Branches)
            val pagerState = rememberPagerState()
            Scaffold(topBar = {
                Column() {
                    RepositoryDetailScreenTopBar(name = repo, onBackClick)
                    if (!isLandScape) {
                        val coroutineScope = rememberCoroutineScope()
                        TabRow(
                            selectedTabIndex = pagerState.currentPage,
                        ) {
                            pages.forEachIndexed { index, page ->
                                Tab(
                                    text = { Text(stringResource(id = page.name)) },
                                    selected = pagerState.currentPage == index,
                                    onClick = {
                                        coroutineScope.launch {
                                            pagerState.scrollToPage(index)
                                        }
                                    },
                                )
                            }
                        }
                    }
                }

            }) { paddingValues ->
                if (isLandScape) {
                    Row(
                        Modifier
                            .padding(paddingValues)
                            .fillMaxSize()
                    ) {
                        Box(modifier = Modifier.fillMaxWidth(0.5f)) {
                            tabPages[0]()
                        }
                        Box(modifier = Modifier.fillMaxWidth()) {
                            tabPages[1]()
                        }
                    }
                } else {
                    HorizontalPager(
                        count = 2,
                        modifier = Modifier
                            .padding(paddingValues)
                            .padding(top = 16.dp),
                        state = pagerState
                    ) {
                        tabPages[pagerState.currentPage]()
                    }
                }


            }
        }
    }
}

@Composable
fun RepositoryDetailScreenTopBar(name: String, onBackClick: () -> Unit) {
    SmallTopAppBar(
        title = { Text(text = name) },
        navigationIcon = {
            IconButton(onClick = onBackClick) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = stringResource(id = R.string.repository_detail_button_back_description)
                )
            }
        }
    )
}


@Composable
fun CommitsSection(
    commits: Resource<List<Commit>>,
    showHeadline: Boolean,
    onRefreshButtonClick: () -> Unit
) {

    Column {
        if (showHeadline) {
            Text(
                text = stringResource(id = R.string.repository_detail_commits_title),
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(start = 16.dp, bottom = 16.dp, top = 8.dp)
            )
        }
        when (commits) {
            is Resource.Loading -> {
                EmptyRepositoryDetailScreen(areItemsLoading = true) {
                    onRefreshButtonClick()
                }
            }
            is Resource.Success -> {
                if (commits.data.isEmpty()) {
                    EmptyDetailScreenSection(message = stringResource(id = R.string.repository_commits_are_empty))
                } else {


                    LazyColumn {
                        items(commits.data) {
                            CommitItem(commit = it)
                        }

                    }
                }
            }
            is Resource.Failure -> {
                EmptyRepositoryDetailScreen(areItemsLoading = false) {
                    onRefreshButtonClick()
                }
            }
        }
    }
}

@Composable
fun EmptyDetailScreenSection(message: String) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = message)
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CommitItem(commit: Commit) {
    Card(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
    ) {
        Column(modifier = Modifier.padding(16.dp)) {
            Text(text = commit.name, style = MaterialTheme.typography.titleMedium)
            Spacer(modifier = Modifier.height(10.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = commit.author, style = MaterialTheme.typography.titleSmall)
                Spacer(modifier = Modifier.width(12.dp))
                CommittedAtSection(committedAt = commit.committedAt)
            }
        }
    }
}


@Composable
fun CommittedAtSection(committedAt: String) {
    Text(
        text = stringResource(id = R.string.repository_detail_committed_at),
        style = MaterialTheme.typography.bodySmall
    )
    Spacer(modifier = Modifier.width(4.dp))
    Text(text = committedAt, style = MaterialTheme.typography.labelLarge)
}

@Composable
fun BranchesSection(
    branches: Resource<List<Branch>>,
    showHeadline: Boolean,
    onRefreshButtonClick: () -> Unit
) {
    Column {
        if (showHeadline) {
            Text(
                text = stringResource(id = R.string.repository_detail_branches_title),
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(start = 16.dp, bottom = 16.dp, top = 8.dp)
            )
        }
        when (branches) {
            is Resource.Loading -> {
                EmptyRepositoryDetailScreen(areItemsLoading = true) {
                    onRefreshButtonClick()
                }
            }
            is Resource.Success -> {
                if (branches.data.isEmpty()) {
                    EmptyDetailScreenSection(message = stringResource(id = R.string.repository_branches_are_empty))
                } else {
                    LazyColumn {
                        items(branches.data) {
                            BranchItem(branch = it)
                        }
                    }
                }
            }
            is Resource.Failure -> {
                EmptyRepositoryDetailScreen(areItemsLoading = false) {
                    onRefreshButtonClick()
                }
            }
        }
    }

}


@Composable
fun EmptyRepositoryDetailScreen(areItemsLoading: Boolean, onRefreshButtonClick: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        if (areItemsLoading) {
            CircularProgressIndicator()
        } else {
            Button(onClick = onRefreshButtonClick) {
                Text(text = stringResource(id = R.string.repository_refresh_button))
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BranchItem(branch: Branch) {
    Card(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxWidth()
    ) {
        Text(
            text = branch.name,
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.padding(16.dp)
        )
    }
}

@Preview
@Composable
fun RepositoryDetailScreenPreview() {
    AppTheme() {
        val commit = remember {
            Commit("Create Detail Screen", "Matyáš", "20 Nov 2022")
        }
        val branch = remember {
            Branch("master")
        }

        val commits = remember {
            listOf(
                commit, commit, commit, commit, commit, commit
            )
        }

        val branches = remember {
            listOf(branch, branch, branch, branch)
        }
        val model = remember {
            RepositoryViewModel.RepositoryScreenModel(
                Resource.Success(branches),
                Resource.Success(commits),
                { _, _ -> },
                { _, _ -> },
                { _, _ -> },
                { }
            )
        }

        RepositoryDetailScreen("", "", model, {})
    }
}
