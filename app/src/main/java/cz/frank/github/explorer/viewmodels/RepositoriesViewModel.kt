package cz.frank.github.explorer.viewmodels

import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.frank.github.explorer.helpers.Resource
import cz.frank.github.explorer.repository.MainRepository
import cz.frank.github.explorer.ui.model.RepositoryShort
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RepositoriesViewModel(private val repository: MainRepository) : ViewModel() {


    private var repositories by mutableStateOf<Resource<List<RepositoryShort>>>(Resource.Loading())
    private var wentBack by mutableStateOf(true)


    private fun initSearch(user: String) = viewModelScope.launch(Dispatchers.IO) {
        if (wentBack) {
            wentBack = false
            repositories = Resource.Loading()
            repositories = repository.getRepositoriesForUser(user)
        }

    }

    private fun refreshSearch(user: String) = viewModelScope.launch(Dispatchers.IO) {
        repositories = Resource.Loading()
        repositories = repository.getRepositoriesForUser(user)
    }

    private fun popBackStack() {
        wentBack = true
    }

    data class RepositoriesScreenModel(
        val repositories: Resource<List<RepositoryShort>>,
        val initSearch: (String) -> Unit,
        val popBackStack: () -> Unit,
        val refreshSearch: (String) -> Unit
    )

    val model by derivedStateOf {
        RepositoriesScreenModel(
            repositories,
            this::initSearch,
            this::popBackStack,
            this::refreshSearch
        )
    }
}
