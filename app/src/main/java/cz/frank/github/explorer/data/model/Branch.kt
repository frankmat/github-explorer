package cz.frank.github.explorer.data.model

import cz.frank.github.explorer.ui.model.BranchUI
import kotlinx.serialization.Serializable

typealias BranchData = Branch

@Serializable
data class Branch(val name: String) : BaseModel<BranchData, BranchUI> {
    override fun toUIModel(): BranchUI {
        return BranchUI(name)
    }
}
