package cz.frank.github.explorer.data.model

import cz.frank.github.explorer.ui.model.CommitUI
import kotlinx.serialization.Serializable

typealias CommitData = CommitHolder

@Serializable
data class CommitHolder(
    val commit: Commit
) : BaseModel<CommitData, CommitUI> {
    override fun toUIModel(): CommitUI {
        return commit.toUIModel()
    }
}
