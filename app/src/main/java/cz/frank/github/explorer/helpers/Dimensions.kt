package cz.frank.github.explorer.helpers

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

sealed class DimensionsUntil(val width: Dp) {
    object Portrait : DimensionsUntil(550.dp)
}
