package cz.frank.github.explorer.helpers

sealed class Resource<T> {
    class Success<T>(val data: T) : Resource<T>()
    class Failure<T>(val failure: cz.frank.github.explorer.helpers.Failure) : Resource<T>()
    class Loading<T> : Resource<T>()
}
