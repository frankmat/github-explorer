package cz.frank.github.explorer

import android.app.Application
import cz.frank.github.explorer.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class GitHubExplorerApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidLogger()
            androidContext(this@GitHubExplorerApplication)
            modules(appModule)
        }
    }
}
