package cz.frank.github.explorer.ui

import androidx.annotation.StringRes
import cz.frank.github.explorer.R

sealed class Tabs(@StringRes val name: Int) {
    object Commits : Tabs(R.string.repository_detail_tab_commits)
    object Branches : Tabs(R.string.repository_detail_tab_branches)
}
