package cz.frank.github.explorer.viewmodels

import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel


class SearchViewModel() : ViewModel() {

    private var isInfoVisible by mutableStateOf(false)
    private var searchQuery by mutableStateOf("")

    private fun onQueryChange(newValue: String) {
        searchQuery = newValue
    }

    private fun onTrailingIconClick() {
        searchQuery = ""
    }

    private fun onInfoButtonCLick() {
        isInfoVisible = !isInfoVisible
    }


    class SearchScreenModel(
        val searchQuery: String,
        val isInfoVisible: Boolean,
        val onInfoButtonClick: () -> Unit,
        val onSearchQueryChange: (String) -> Unit,
        val onTrailingIconClick: () -> Unit
    )

    val model by derivedStateOf {
        SearchScreenModel(
            searchQuery,
            isInfoVisible,
            this::onInfoButtonCLick,
            this::onQueryChange,
            this::onTrailingIconClick
        )
    }
}
