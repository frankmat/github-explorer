package cz.frank.github.explorer.repository

import cz.frank.github.explorer.helpers.Resource
import cz.frank.github.explorer.ui.model.Branch
import cz.frank.github.explorer.ui.model.Commit
import cz.frank.github.explorer.ui.model.RepositoryShort

interface MainRepository {
    suspend fun getRepositoriesForUser(user: String): Resource<List<RepositoryShort>>
    suspend fun getRepositoryBranchesForUser(
        repository: String,
        user: String
    ): Resource<List<Branch>>

    suspend fun getRepositoryCommitsForUser(
        repository: String,
        user: String
    ): Resource<List<Commit>>
}
