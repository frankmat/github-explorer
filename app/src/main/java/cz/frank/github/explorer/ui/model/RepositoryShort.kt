package cz.frank.github.explorer.ui.model

typealias RepositoryShortUI = RepositoryShort

data class RepositoryShort(
    val name: String,
    val description: String,
    val language: String,
    val forksCount: Int,
    val updatedAt: String
)
