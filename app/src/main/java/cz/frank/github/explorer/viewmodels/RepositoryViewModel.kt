package cz.frank.github.explorer.viewmodels

import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.frank.github.explorer.helpers.Resource
import cz.frank.github.explorer.repository.MainRepository
import cz.frank.github.explorer.ui.model.Branch
import cz.frank.github.explorer.ui.model.Commit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RepositoryViewModel(private val mainRepository: MainRepository) : ViewModel() {


    private var commits by mutableStateOf<Resource<List<Commit>>>(Resource.Loading())
    private var branches by mutableStateOf<Resource<List<Branch>>>(Resource.Loading())
    private var wentBack by mutableStateOf(true)

    private fun initSearch(repo: String, user: String) {
        if (wentBack) {
            wentBack = false
            commitsSearch(repo, user)
            branchesSearch(repo, user)
        }
    }

    private fun commitsSearch(repo: String, user: String) {
        viewModelScope.launch(Dispatchers.IO) {
            commits = Resource.Loading()
            val newCommits = mainRepository.getRepositoryCommitsForUser(repo, user)
            commits = newCommits
        }
    }

    private fun branchesSearch(repo: String, user: String) {
        viewModelScope.launch(Dispatchers.IO) {
            branches = Resource.Loading()
            val newBranches = mainRepository.getRepositoryBranchesForUser(repo, user)
            branches = newBranches
        }
    }

    private fun popBackStack() {
        wentBack = true
    }


    data class RepositoryScreenModel(
        val branches: Resource<List<Branch>>,
        val commits: Resource<List<Commit>>,
        val initSearch: (String, String) -> Unit,
        val onRefreshBranchesButtonClick: (String, String) -> Unit,
        val onRefreshCommitsButtonClick: (String, String) -> Unit,
        val popBackStack: () -> Unit,


        )

    val model by derivedStateOf {
        RepositoryScreenModel(
            branches,
            commits,
            this::initSearch,
            this::branchesSearch,
            this::commitsSearch,
            this::popBackStack
        )
    }
}
