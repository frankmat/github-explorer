package cz.frank.github.explorer.ui.model

typealias BranchUI = Branch

data class Branch(val name: String)
