package cz.frank.github.explorer.data

import cz.frank.github.explorer.helpers.Endpoints
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*


class GitHubApi(private val client: HttpClient) {

    suspend fun getUserRepositories(
        user: String
    ): HttpResponse = client.get(Endpoints.getUserRepositoriesEndpoint(user))

    suspend fun getRepositoryBranches(
        repository: String,
        user: String
    ): HttpResponse = client.get(Endpoints.getRepositoryBranchesEndpoint(user, repository))

    suspend fun getRepositoryCommits(
        repository: String,
        user: String
    ): HttpResponse = client.get(Endpoints.getRepositoryCommitsEndpoint(user, repository))
}
