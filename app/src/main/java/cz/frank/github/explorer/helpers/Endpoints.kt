package cz.frank.github.explorer.helpers

object Endpoints {
    private const val BASIC_ENDPOINT = "https://api.github.com"
    private const val COMMIT_PER_PAGE = 10
    private const val COMMIT_PAGE = 1
    fun getUserRepositoriesEndpoint(user: String) = "$BASIC_ENDPOINT/users/$user/repos"
    fun getRepositoryCommitsEndpoint(user: String, repository: String) =
        "$BASIC_ENDPOINT/repos/$user/$repository/commits?page=$COMMIT_PAGE&per_page=$COMMIT_PER_PAGE"

    fun getRepositoryBranchesEndpoint(user: String, repository: String) =
        "$BASIC_ENDPOINT/repos/$user/$repository/branches"
}
