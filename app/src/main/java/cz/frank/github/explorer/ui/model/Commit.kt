package cz.frank.github.explorer.ui.model

typealias CommitUI = Commit

data class Commit(val name: String, val author: String, val committedAt: String)
