package cz.frank.github.explorer.helpers

enum class Failure {
    NotExistingUser, DoNotCare
}

fun Int?.toFailure(): Failure {
    return if (this == 404) {
        Failure.NotExistingUser
    } else {
        Failure.DoNotCare
    }
}
