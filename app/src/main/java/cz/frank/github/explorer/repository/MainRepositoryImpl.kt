package cz.frank.github.explorer.repository

import cz.frank.github.explorer.data.GitHubApi
import cz.frank.github.explorer.data.model.BaseModel
import cz.frank.github.explorer.data.model.BranchData
import cz.frank.github.explorer.data.model.CommitData
import cz.frank.github.explorer.data.model.RepositoryShortData
import cz.frank.github.explorer.helpers.Resource
import cz.frank.github.explorer.helpers.toFailure
import cz.frank.github.explorer.ui.model.*
import io.ktor.client.call.*
import io.ktor.client.statement.*


class MainRepositoryImpl(private val api: GitHubApi) : MainRepository {


    override suspend fun getRepositoriesForUser(user: String): Resource<List<RepositoryShort>> =
        getData<RepositoryShortData, RepositoryShortUI> {
            api.getUserRepositories(user)
        }


    override suspend fun getRepositoryBranchesForUser(
        repository: String,
        user: String
    ): Resource<List<Branch>> =
        getData<BranchData, BranchUI> {
            api.getRepositoryBranches(repository, user)
        }

    override suspend fun getRepositoryCommitsForUser(
        repository: String,
        user: String
    ): Resource<List<Commit>> =
        getData<CommitData, CommitUI> {
            api.getRepositoryCommits(repository, user)
        }


    private suspend inline fun <reified DATA_MODEL, UI_MODEL> getData(
        noinline getResponse: suspend () -> HttpResponse
    ): Resource<List<UI_MODEL>> where DATA_MODEL : BaseModel<DATA_MODEL, UI_MODEL> {
        var responseCode: Int? = null
        return try {
            val response = getResponse()
            responseCode = response.status.value
            val body: List<DATA_MODEL> = response.body()
            Resource.Success(body.map { it.toUIModel() })
        } catch (e: Exception) {
            Resource.Failure(responseCode.toFailure())
        }
    }
}
