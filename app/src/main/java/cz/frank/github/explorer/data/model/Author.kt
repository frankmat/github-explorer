package cz.frank.github.explorer.data.model

import kotlinx.serialization.Serializable

@Serializable
data class Author(val name: String, val date: String)
