package cz.frank.github.explorer.ui.screens

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.frank.github.explorer.R
import cz.frank.github.explorer.helpers.Failure
import cz.frank.github.explorer.helpers.Resource
import cz.frank.github.explorer.ui.model.RepositoryShort
import cz.frank.github.explorer.ui.screens.destinations.RepositoryDetailScreenHolderDestination
import cz.frank.github.explorer.ui.theme.AppTheme
import cz.frank.github.explorer.viewmodels.RepositoriesViewModel
import org.koin.androidx.compose.getViewModel


@Destination
@Composable
fun RepositoriesScreenHolder(
    user: String,
    navigator: DestinationsNavigator,
    viewModel: RepositoriesViewModel = getViewModel()
) {
    with(viewModel.model) {
        BackHandler(true) {
            popBackStack()
            navigator.popBackStack()
        }
        LaunchedEffect(Unit) {
            initSearch(user)
        }
        RepositoriesScreen(
            user,
            repositories,
            { navigator.popBackStack() },
            {
                navigator.navigate(
                    RepositoryDetailScreenHolderDestination(user, it)
                )
            },
            { refreshSearch(user) }
        )
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RepositoriesScreen(
    searchedUser: String,
    repositories: Resource<List<RepositoryShort>>,
    onBackClick: () -> Unit,
    onRepositoryClick: (String) -> Unit,
    onRefreshButtonClick: () -> Unit
) {
    Scaffold(topBar = {
        RepositoriesScreenTopBar(
            when (repositories) {
                is Resource.Loading -> searchedUser
                is Resource.Success -> searchedUser
                is Resource.Failure -> if (repositories.failure == Failure.NotExistingUser) stringResource(
                    id = R.string.repositories_user_not_found
                ) else searchedUser
            },
            onBackClick
        )
    }) { paddingValues ->

        when (repositories) {
            is Resource.Loading -> {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    CircularProgressIndicator()
                }
            }
            is Resource.Success -> {
                if (repositories.data.isEmpty()) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Text(text = stringResource(id = R.string.repositories_no_public_repos))
                    }
                } else {
                    LazyColumn(
                        modifier = Modifier
                            .padding(paddingValues)
                            .fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally,

                        ) {
                        items(repositories.data) {
                            RepositoryItem(it, onRepositoryClick)
                        }
                    }
                }
            }
            is Resource.Failure -> when (repositories.failure) {
                Failure.NotExistingUser -> {
                    //intentional empty screen
                }
                Failure.DoNotCare -> {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.Center
                    ) {
                        Button(onClick = onRefreshButtonClick) {
                            Text(text = stringResource(id = R.string.repository_refresh_button))
                        }
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RepositoryItem(repository: RepositoryShort, onRepositoryClick: (String) -> Unit) {
    Card(
        modifier = Modifier
            .padding(start = 8.dp, top = 8.dp, end = 8.dp)
            .fillMaxWidth(),
        onClick = { onRepositoryClick(repository.name) }
    ) {
        Column(modifier = Modifier.padding(18.dp)) {
            Text(text = repository.name, style = MaterialTheme.typography.titleLarge)
            Spacer(modifier = Modifier.height(6.dp))
            Text(
                text = repository.description,
                style = MaterialTheme.typography.bodyMedium,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
            Spacer(modifier = Modifier.height(6.dp))
            Row(verticalAlignment = Alignment.CenterVertically) {
                RepositoryItemLanguage(repository.language)
                Spacer(modifier = Modifier.width(12.dp))
                RepositoryItemForks(repository.forksCount)
                Spacer(modifier = Modifier.width(12.dp))
                RepositoryItemUpdatedAt(repository.updatedAt)
            }
        }
    }
}

@Composable
fun RepositoryItemLanguage(language: String) {
    if (language.isNotBlank()) {
        Text(
            text = stringResource(id = R.string.repositories_language),
            style = MaterialTheme.typography.bodySmall
        )
        Spacer(modifier = Modifier.width(4.dp))
        Text(text = language, style = MaterialTheme.typography.labelLarge)
    }
}

@Composable
fun RepositoryItemForks(forksCount: Int) {
    Icon(
        painterResource(id = R.drawable.ic_git_fork),
        contentDescription = stringResource(id = R.string.repositories_forks_icon)
    )
    Spacer(modifier = Modifier.width(4.dp))
    Text(text = forksCount.toString(), style = MaterialTheme.typography.labelLarge)
}

@Composable
fun RepositoryItemUpdatedAt(updatedAt: String) {
    Text(
        text = stringResource(id = R.string.repositories_updated_at),
        style = MaterialTheme.typography.bodySmall
    )
    Spacer(modifier = Modifier.width(4.dp))
    Text(text = updatedAt, style = MaterialTheme.typography.labelLarge)
}

@Composable
fun RepositoriesScreenTopBar(user: String, onBackClick: () -> Unit) {
    SmallTopAppBar(
        title = { Text(text = user) },
        navigationIcon = {
            IconButton(onClick = onBackClick) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = stringResource(id = R.string.repositories_button_back_description)
                )
            }
        }
    )
}

@Preview
@Composable
fun RepositoriesScreenPreview() {
    AppTheme() {


        val shortRepo = remember {
            RepositoryShort(
                "znamky",
                "Popis turistických známek",
                "Kotlin",
                3,
                "20 Nov 2022"
            )
        }

        val longRepo = remember {
            RepositoryShort(
                "znamky",
                "Popis turistických známek,Popis turistických známekPopis turistických známek,",
                "Kotlin",
                3,
                "20 Nov 2022"
            )
        }
        val repositories = remember {
            listOf(
                shortRepo,
                longRepo,
                shortRepo,
                shortRepo,
                shortRepo,
                shortRepo,
                shortRepo,
                shortRepo
            )
        }


        RepositoriesScreen("Matyáš", Resource.Success(repositories), {}, {}, { })
    }
}
