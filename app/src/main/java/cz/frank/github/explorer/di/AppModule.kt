package cz.frank.github.explorer.di

import cz.frank.github.explorer.data.GitHubApi
import cz.frank.github.explorer.ktorHttpClient
import cz.frank.github.explorer.repository.MainRepository
import cz.frank.github.explorer.repository.MainRepositoryImpl
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import cz.frank.github.explorer.viewmodels.*

val appModule = module {
    singleOf(::ktorHttpClient)
    singleOf(::GitHubApi)
    singleOf(::MainRepositoryImpl){bind<MainRepository>()}
    viewModelOf(::SearchViewModel)
    viewModelOf(::RepositoriesViewModel)
    viewModelOf(::RepositoryViewModel)
}
