package cz.frank.github.explorer.data.model

import cz.frank.github.explorer.helpers.DateConstants
import cz.frank.github.explorer.helpers.transformDate
import cz.frank.github.explorer.ui.model.RepositoryShortUI
import kotlinx.serialization.Serializable

typealias RepositoryShortData = RepositoryShort

@Serializable
data class RepositoryShort(
    val name: String,
    val description: String?,
    val language: String?,
    val forks_count: Int,
    val updated_at: String
) : BaseModel<RepositoryShortData, RepositoryShortUI> {
    override fun toUIModel(): RepositoryShortUI {
        val date =
            transformDate(
                DateConstants.ISO8601_DATE_FORMAT,
                DateConstants.OUTPUT_FORMAT,
                updated_at
            )
        return RepositoryShortUI(name, description ?: "", language ?: "", forks_count, date)
    }
}
