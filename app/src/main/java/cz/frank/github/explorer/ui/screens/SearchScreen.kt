package cz.frank.github.explorer.ui.screens

import androidx.compose.animation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.frank.github.explorer.R
import cz.frank.github.explorer.ui.screens.destinations.RepositoriesScreenHolderDestination
import cz.frank.github.explorer.ui.theme.AppTheme
import cz.frank.github.explorer.viewmodels.SearchViewModel
import org.koin.androidx.compose.getViewModel

@Destination(start = true)
@Composable
fun SearchScreenHolder(
    navigator: DestinationsNavigator,
    viewModel: SearchViewModel = getViewModel()
) {
    SearchScreen(
        onSearchClick = { navigator.navigate(RepositoriesScreenHolderDestination(viewModel.model.searchQuery)) },
        viewModel.model
    )
}


@OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)
@Composable
fun SearchScreen(
    onSearchClick: () -> Unit,
    searchScreenModel: SearchViewModel.SearchScreenModel
) {
    with(searchScreenModel) {
        Scaffold(
            topBar = { SearchTopBar(onInfoButtonClick, isInfoVisible) },
            modifier = Modifier.fillMaxSize()
        ) { paddingValues ->
            Box(
                modifier = Modifier
                    .padding(paddingValues)
            ) {
                SearchScreenForm(
                    onSearchClick,
                    onSearchQueryChange,
                    searchQuery,
                    onTrailingIconClick
                )
                AnimatedVisibility(
                    visible = isInfoVisible,
                    enter = scaleIn(transformOrigin = TransformOrigin(1f, 0f)),
                    exit = scaleOut(transformOrigin = TransformOrigin(1f, 0f))
                ) {
                    SearchScreenInfoCard()
                }
            }
        }
    }

}


@Composable
fun SearchScreenForm(
    onSearchClick: () -> Unit,
    onSearchQueryChange: (String) -> Unit,
    searchQuery: String,
    onTrailingIconClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        TextField(
            value = searchQuery,
            onValueChange = onSearchQueryChange,
            label = {
                Text(
                    text = stringResource(id = R.string.search_textField_label)
                )
            },
            trailingIcon = {
                IconButton(onClick = onTrailingIconClick) {
                    Icon(
                        imageVector = Icons.Default.Clear,
                        contentDescription = stringResource(id = R.string.search_button_trailing_icon_description)
                    )
                }
            }
        )
        Spacer(modifier = Modifier.height(32.dp))
        Button(onClick = onSearchClick) {
            Text(text = stringResource(id = R.string.search_button_search))
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreenInfoCard() {
    Card(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(
                text = stringResource(id = R.string.search_about_info),
                style = MaterialTheme.typography.bodyLarge,
                textAlign = TextAlign.Center
            )
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    text = stringResource(id = R.string.search_about_created_by),
                    style = MaterialTheme.typography.bodyMedium
                )
                Text(
                    text = stringResource(id = R.string.author),
                    style = MaterialTheme.typography.labelLarge
                )
            }

        }
    }
}

@Composable
fun SearchTopBar(onInfoClick: () -> Unit, isInfoVisible: Boolean) {
    SmallTopAppBar(
        title = { Text(stringResource(id = R.string.app_name)) },
        actions = {
            SearchScreenTopBarActions(onInfoClick, isInfoVisible)
        }
    )
}

@Composable
fun SearchScreenTopBarActions(onInfoClick: () -> Unit, isInfoVisible: Boolean) {

    IconButton(onClick = onInfoClick) {
        Crossfade(isInfoVisible) {
            if (it) {
                Icon(
                    Icons.Default.Close,
                    contentDescription = stringResource(id = R.string.search_button_info_description)
                )
            } else {
                Icon(
                    Icons.Default.Info,
                    contentDescription = stringResource(id = R.string.search_button_info_description)
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun SearchScreenPreview() {
    AppTheme {
        val showInfo = remember {
            mutableStateOf(false)
        }
        val model = SearchViewModel.SearchScreenModel(
            searchQuery = "",
            isInfoVisible = showInfo.value,
            onInfoButtonClick = { showInfo.value = !showInfo.value },
            onSearchQueryChange = {},
            onTrailingIconClick = {}
        )
        SearchScreen({}, model)
    }
}
