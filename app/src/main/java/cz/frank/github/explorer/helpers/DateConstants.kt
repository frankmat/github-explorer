package cz.frank.github.explorer.helpers

object DateConstants {
    const val ISO8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    const val OUTPUT_FORMAT = "yyyy MM dd"
}
