package cz.frank.github.explorer.data.model

interface BaseModel<DATA_MODEL, UI_MODEL> {
    fun toUIModel(): UI_MODEL
}
