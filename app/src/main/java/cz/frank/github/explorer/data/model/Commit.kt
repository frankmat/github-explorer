package cz.frank.github.explorer.data.model

import cz.frank.github.explorer.helpers.DateConstants
import cz.frank.github.explorer.helpers.transformDate
import cz.frank.github.explorer.ui.model.CommitUI
import kotlinx.serialization.Serializable

@Serializable
data class Commit(val message: String, val author: Author) {
    fun toUIModel(): CommitUI {
        val date =
            transformDate(
                DateConstants.ISO8601_DATE_FORMAT,
                DateConstants.OUTPUT_FORMAT,
                author.date
            )
        return CommitUI(message, author.name, date)
    }
}
